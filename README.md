# Simple Logger for Go inspired by log4j 

[![pipeline status](https://gitlab.com/protocod/log/badges/master/pipeline.svg)](https://gitlab.com/protocod/log/commits/master) [![coverage report](https://gitlab.com/protocod/log/badges/master/coverage.svg)](https://gitlab.com/protocod/log/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/protocod/log?status.svg)](https://godoc.org/gitlab.com/protocod/log)

## How to use ?

Use the static method `log.Instance()` to get an instance of `log.logger` structure for the current file.

``` go
package main

import (
	"bytes"
	"fmt"
	"os"

	"gitlab.com/protocod/log"
)

func main() {
	log.FATAL.Enable = false // disable FATAL logs

	logger := log.Instance()  // get unique logger instance for current file, like a singleton
	logger.Buffer = os.Stdout // set default buffer io.Writer to stdout

	logger.Trace("I'm working") // Explicit TRACE log call
	logger.Info("Have a nice day")
	logger.Debug("Do you have a problem ?")
	logger.Error("Ouch")
	logger.Fatal("AAAH")

	logger.SetLevel(log.INFO) // set Logger on log.INFO by default
	logger.Log("How's life ?")

	var myBuffer bytes.Buffer
	logger.Flog(&myBuffer, log.TRACE, "Another trace message") // provide your own io.Writer object
	fmt.Println(myBuffer.String())
}
```

Output

```
2019-05-15 20:35:57.058144932 +0000 UTC [TRACE] (/home/maximilien/go/src/gitlab.com/protocod/hw/main.go:17) I'm working
2019-05-15 20:35:57.058377561 +0000 UTC [INFO] (/home/maximilien/go/src/gitlab.com/protocod/hw/main.go:18) Have a nice day
2019-05-15 20:35:57.058429418 +0000 UTC [DEBUG] (/home/maximilien/go/src/gitlab.com/protocod/hw/main.go:19) Do you have a problem ?
2019-05-15 20:35:57.058497434 +0000 UTC [ERROR] (/home/maximilien/go/src/gitlab.com/protocod/hw/main.go:20) Ouch
2019-05-15 20:35:57.058539558 +0000 UTC [INFO] (/home/maximilien/go/src/gitlab.com/protocod/hw/main.go:24) How's life ?
2019-05-15 20:35:57.058592545 +0000 UTC [TRACE] (/home/maximilien/go/src/gitlab.com/protocod/hw/main.go:27) Another trace message
```

The logger is created using the filename (main.go in this example) using `runtime.Caller` function.

## Log Level

|name| use case|
|----|------------|
|log.TRACE|To print stack trace like input and output of methods|
|log.INFO|To print information messages|
|log.DEBUG|To print data which help you to debug|
|log.WARN|To print wrong data or execution during process|
|log.ERROR|To print handled error|
|log.FATAL|To print unhandled or critical error|
