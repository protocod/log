package log

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

func TestNewLevel(t *testing.T) {
	level := newLevel(trace, true, "TRACE")

	if level.value == trace && level.Enable {
		t.Logf("OK")
	} else {
		t.Errorf("%v", level)
	}
}

func TestLevelEnum(t *testing.T) {
	condition := trace == 0 && debug == 1 && info == 2 && warn == 3 && err == 4 && fatal == 5

	if condition {
		t.Log("OK")
	} else {
		t.Errorf("wrong levelEnum, trace %d, debug %d, info %d, warn %d, err %d, fatal %d", trace, debug, info, warn, err, fatal)
	}
}

func testLoggerInstanceDeeper(t *testing.T, logger *Logger) {
	filepath := logger.file

	if filepath == "" {
		t.Errorf("No filepath found on logger %v", logger)
	}

	splitted := strings.Split(filepath, "/")

	if len(splitted) == 0 {
		t.Errorf("Invalid filepath %s", filepath)
	}

	filename := splitted[len(splitted)-1]

	if filename != "logger_test.go" {
		t.Errorf("Invalid filename %s", filename)
	}

	if logger.line != 0 {
		t.Errorf("Invalid line %d", logger.line)
	}

	if logger.level != nil {
		t.Errorf("Invalid level %v", logger.level)
	}
}

func TestInstance(t *testing.T) {
	logger := Instance()

	testLoggerInstanceDeeper(t, logger)

	logger2 := Instance()

	if reflect.DeepEqual(logger, logger2) {
		t.Logf("OK")
	} else {
		t.Errorf("logger and logger2 not equal\nlogger: %v\nlogger2: %v", logger, logger2)
	}
}

func TestSetAndGetLevel(t *testing.T) {
	logger := Instance()
	logger.SetLevel(TRACE)

	if reflect.DeepEqual(logger.GetLevel(), TRACE) {
		t.Logf("OK")
	} else {
		t.Errorf("Invalid level %v must be equal to %v", logger.level, TRACE)
	}
}

func testLog(t *testing.T, level *Level) {
	loggers := Instance().SetLevel(level)
	var buffer bytes.Buffer
	loggers.Buffer = &buffer

	loggers.Log("test %d", 42)

	output := buffer.String()
	condition := strings.Contains(output, fmt.Sprintf(" [%s] ", level.name)) && strings.Contains(output, "logger_test.go:89") && strings.Contains(output, "test 42")

	if condition {
		t.Logf("OK")
	} else {
		t.Errorf("Invalid log, output: %s", output)
	}
}

type logCallback func(format string, a ...interface{})

func testLogUsingCallback(t *testing.T, level *Level, log logCallback, line int) {
	loggers := Instance()
	var buffer bytes.Buffer
	loggers.Buffer = &buffer

	log("test %d", 42)

	output := buffer.String()
	condition := strings.Contains(output, fmt.Sprintf(" [%s] ", level.name)) && strings.Contains(output, "logger_test.go:") && strings.Contains(output, "test 42")

	if condition {
		t.Logf("OK")
	} else {
		t.Errorf("Invalid log, output: %s", output)
	}
}

func testLogWithDisabledLog(t *testing.T, level *Level, log logCallback) {
	loggers := Instance()
	var buffer bytes.Buffer
	loggers.Buffer = &buffer

	log("test %d", 42)

	output := buffer.String()
	condition := len(output) == 0

	if condition {
		t.Logf("OK")
	} else {
		t.Errorf("Invalid log, output: %s", output)
	}
}

func testFlog(t *testing.T, level *Level) {
	loggers := Instance().SetLevel(level)
	var buffer bytes.Buffer
	loggers.Buffer = &buffer

	loggers.Flog(&buffer, level, "test %d", 42)

	output := buffer.String()
	condition := strings.Contains(output, fmt.Sprintf(" [%s] ", level.name)) && strings.Contains(output, "logger_test.go:142") && strings.Contains(output, "test 42")

	if condition {
		t.Logf("OK")
	} else {
		t.Errorf("Invalid log, output: %s", output)
	}
}

func TestLog(t *testing.T) {
	loggers := Instance()

	testLog(t, TRACE)
	testLog(t, DEBUG)
	testLog(t, INFO)
	testLog(t, WARN)
	testLog(t, ERROR)
	testLog(t, FATAL)

	testLogUsingCallback(t, TRACE, loggers.Trace, 130)
	testLogUsingCallback(t, DEBUG, loggers.Debug, 131)
	testLogUsingCallback(t, INFO, loggers.Info, 132)
	testLogUsingCallback(t, WARN, loggers.Warn, 133)
	testLogUsingCallback(t, ERROR, loggers.Error, 134)
	testLogUsingCallback(t, FATAL, loggers.Fatal, 135)

	testFlog(t, TRACE)
	testFlog(t, DEBUG)
	testFlog(t, INFO)
	testFlog(t, WARN)
	testFlog(t, ERROR)
	testFlog(t, FATAL)
}

func TestAllLevelDisable(t *testing.T) {
	loggers := Instance()

	TRACE.Enable = false
	DEBUG.Enable = false
	INFO.Enable = false
	WARN.Enable = false
	ERROR.Enable = false
	FATAL.Enable = false

	testLogWithDisabledLog(t, TRACE, loggers.Trace)
	testLogWithDisabledLog(t, DEBUG, loggers.Debug)
	testLogWithDisabledLog(t, INFO, loggers.Info)
	testLogWithDisabledLog(t, WARN, loggers.Warn)
	testLogWithDisabledLog(t, ERROR, loggers.Error)
	testLogWithDisabledLog(t, FATAL, loggers.Fatal)
}
