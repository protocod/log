// Package log provide a simple logger for golang inspired by log4j
package log

import (
	"errors"
	"fmt"
	"io"
	"os"
	"runtime"
	"sync"
	"time"
)

type levelEnum int

const (
	trace levelEnum = iota
	debug
	info
	warn
	err
	fatal
)

// Level struct which represent a log level
type Level struct {
	value  levelEnum
	Enable bool
	name   string
}

func newLevel(value levelEnum, state bool, name string) *Level {
	level := new(Level)
	level.value = value
	level.Enable = state
	level.name = name
	return level
}

var (
	// TRACE log level
	TRACE = newLevel(trace, true, "TRACE")
	// DEBUG log level
	DEBUG = newLevel(debug, true, "DEBUG")
	// INFO log level
	INFO = newLevel(info, true, "INFO")
	// WARN log level
	WARN = newLevel(warn, true, "WARN")
	// ERROR log level
	ERROR = newLevel(err, true, "ERROR")
	// FATAL log level
	FATAL = newLevel(fatal, true, "FATAL")
)

// Logger struct which represent logger
type Logger struct {
	level  *Level
	file   string
	line   int
	mux    sync.Mutex
	Buffer io.Writer
}

// Contains unique instance of each logger
var loggers = map[string]*Logger{}

// Instance get logger instance, create one if not exist for file
func Instance() *Logger {
	_, file, _, ok := runtime.Caller(1)

	if ok == false {
		panic("Unable to get result from runtime.Caller")
	}

	log, isPresent := loggers[file]

	if isPresent {
		return log
	}

	created := new(Logger)
	created.file = file
	loggers[file] = created

	return created
}

// Free logger memory. Go use a Garbage Collector, it's not determinstic
func Free() error {
	_, file, _, ok := runtime.Caller(1)

	if !ok {
		panic("Unable to get result from runtime.Caller")
	}

	log, isPresent := loggers[file]

	if !isPresent || log == nil {
		return errors.New("Unable to find logger to free")
	}

	log = nil
	return nil
}

// SetLevel of logger
func (l *Logger) SetLevel(level *Level) *Logger {
	l.level = level
	return l
}

// GetLevel get logger lovel
func (l *Logger) GetLevel() *Level {
	return l.level
}

// Log method to make logs
func (l *Logger) Log(format string, a ...interface{}) {
	switch l.level.value {
	case trace:
		l.log(l.defaultBuffer(os.Stdout), TRACE, format, a...)
		break
	case debug:
		l.log(l.defaultBuffer(os.Stdout), DEBUG, format, a...)
		break
	case info:
		l.log(l.defaultBuffer(os.Stdout), INFO, format, a...)
		break
	case warn:
		l.log(l.defaultBuffer(os.Stdout), WARN, format, a...)
		break
	case err:
		l.log(l.defaultBuffer(os.Stderr), ERROR, format, a...)
		break
	case fatal:
		l.log(l.defaultBuffer(os.Stderr), FATAL, format, a...)
		break
	default:
		l.log(l.defaultBuffer(os.Stdout), TRACE, format, a...)
	}
}

// Used by other functions
func (l *Logger) log(writer io.Writer, level *Level, format string, a ...interface{}) {
	if level.Enable == false {
		return
	}

	l.updateDataStacked(3)
	toPrint := fmt.Sprintf("%s [%s] (%s:%d) %s\n", time.Now().UTC().String(), level.name, l.file, l.line, fmt.Sprintf(format, a...))
	defer l.mux.Unlock()
	l.mux.Lock()
	fmt.Fprintf(writer, toPrint)
}

// Flog like Log function but you have to provide your own io.Writer
func (l *Logger) Flog(writer io.Writer, level *Level, format string, a ...interface{}) {
	l.log(writer, level, format, a...)
}

// Trace explicit TRACE log call
func (l *Logger) Trace(format string, a ...interface{}) {
	l.log(l.defaultBuffer(os.Stdout), TRACE, format, a...)
}

// Debug explicit DEBUG log call
func (l *Logger) Debug(format string, a ...interface{}) {
	l.log(l.defaultBuffer(os.Stdout), DEBUG, format, a...)
}

// Info explicit INFO log call
func (l *Logger) Info(format string, a ...interface{}) {
	l.log(l.defaultBuffer(os.Stdout), INFO, format, a...)
}

// Warn explicit WARN log call
func (l *Logger) Warn(format string, a ...interface{}) {
	l.log(l.defaultBuffer(os.Stdout), WARN, format, a...)
}

// Error explicit ERROR log call
func (l *Logger) Error(format string, a ...interface{}) {
	l.log(l.defaultBuffer(os.Stderr), ERROR, format, a...)
}

// Fatal explicit FATAL log call
func (l *Logger) Fatal(format string, a ...interface{}) {
	l.log(l.defaultBuffer(os.Stderr), FATAL, format, a...)
}

func (l *Logger) defaultBuffer(buffer io.Writer) io.Writer {
	if l.Buffer == nil {
		return buffer
	}
	return l.Buffer
}

func (l *Logger) updateDataStacked(level int) {
	_, file, line, ok := runtime.Caller(level)

	if ok == false {
		panic("Unable to get result from runtime.Caller")
	}

	l.line = line
	l.file = file
}
